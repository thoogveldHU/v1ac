import numpy as np


def vector_addition(u: np.ndarray, v: np.ndarray) -> np.ndarray:  # TODO
    if u.size != v.size:
        raise DimensionError("Vectors niet hetzelfde formaat.")
    w = np.array((0, 0, 0))
    for i in range(u.size):
        w[i] = v[i] + u[i]
    return w


def inner_product(u: np.ndarray, v: np.ndarray) -> np.ndarray:
    if u.size != v.size:
        raise DimensionError("Vectors niet hetzelfde formaat.")
    tempInt = 0
    for i in range(u.size):
        tempInt += u[i] * v[i]
    return tempInt


def matrix_product1(M: np.ndarray, v: np.ndarray) -> np.ndarray:  # TODO
    # if len(M) != len(v):
    #     raise DimensionError
    tempM = M
    for i in range(v.size):
        tempM[i] = M[i] * v[i]
    newV = np.ndarray(shape=(1, len(tempM)), dtype=int)
    for i in range(len(tempM)):
        newV[0][i] = tempM[0][i] + tempM[1][i] + tempM[2][i]
    return newV


def matrix_product(M: np.ndarray, N: np.ndarray) -> np.ndarray:
    if M.shape[1] != N.shape[0]:
        raise DimensionError("Kan niet.")

    result = np.ndarray(shape=(3, 3), dtype=int)
    result = [[sum(a*b for a, b in zip(M_row, N_col))
               for N_col in zip(*N)] for M_row in M]
    return result


def determinant_2(M: np.ndarray) -> int:  # TODO
    if type(M) is int:
        return M
    if M.shape[0] != M.shape[1] or M.shape[0] != 2:
        raise DimensionError("Niet de juiste verhouding.")
    return (M[0][0] * M[1][1]) - (M[0][1] * M[1][0])


def determinant_3(M: np.ndarray) -> int:  # TODO
    if type(M) is int:
        return M
    if M.shape[0] != M.shape[1] or M.shape[0] != 3:
        raise DimensionError("Niet de juiste verhouding.")
    # Make the list biggger with 2 cols.
    N = np.zeros(shape=(3, 5), dtype=int)
    for i in range(M.shape[0]):
        for j in range(M.shape[1]):
            N[i][j] = M[i][j]

    # fill the cols with data from the old matrix
    for i in range(3):
        for j in range(2):
            N[i][j + 3] = M[i][j]

    # calculate using rule of sarrus
    a = (N[0][0] * N[1][1] * N[2][2]) + (N[0][1] * N[1]
                                         [2] * N[2][3]) + (N[0][2] * N[1][3] * N[2][4])
    b = (N[0][2] * N[1][1] * N[2][0]) + (N[0][3] * N[1]
                                         [2] * N[2][1]) + (N[0][4] * N[1][3] * N[2][2])
    return a - b


def determinant2(M: np.ndarray) -> int:  # TODO
    try:
        return int(M)
    except TypeError:
        if M.shape[0] != M.shape[1]:
            raise DimensionError("Niet de juiste verhouding.")
        if M.shape[0] == 2:
            return determinant_2(M)
        elif M.shape[0] == 3:
            return determinant_3(M)
        elif (M.shape[0] == 1):
            return M[0][0]


def inverse_matrix_2(M: np.ndarray) -> np.ndarray:  # TODO
    det = determinant2(M)
    if det == 0:
        raise NonInvertibleError("Determinant is 0")

    # make new matrix to fill in.
    adj = np.zeros(shape=(2, 2))

    # swap values with old and inverse the dia line.
    adj[0][0], adj[1][1] = M[1][1], M[0][0]
    adj[0][1], adj[1][0] = M[0][1] * -1, M[1][0] * -1

    # calulate new values in matrix
    for i in range(2):
        for j in range(2):
            adj[i][j] = adj[i][j] / det

    return adj


def sign(x: int) -> int:
    """TODO: Documenteer mij!"""
    if x % 2 == 0:
        return 1
    else:
        return -1


def submatrix(M: np.ndarray, x: int, y: int) -> np.ndarray:
    """TODO: Documenteer mij!"""
    return np.delete(np.delete(M, x, 0), y, 1)


def determinant(M: np.ndarray) -> float:
    """TODO: Documenteer mij!"""

    # Check for more than 1 item
    if M.shape == () or M.shape == (0, 0) or M.shape == (1, 1):
        return M.item()

    # check for equal size
    elif M.shape[0] != M.shape[1]:
        raise DimensionError("De matrix is niet vierkant")

    # onnodige else.
    else:
        # Det op 0
        det = 0
        # for item in range of the size of the matrix
        for y in range(M.shape[1]):
            # keep adding int value to the det, depending on the values
            det += sign(y) * M[0][y] * determinant(submatrix(M, 0, y))
        return det


def cofactor_matrix(M: np.ndarray) -> np.ndarray:
    """TODO: Documenteer mij!"""
    adj = np.zeros((M.shape[0], M.shape[1]))
    for x in range(M.shape[0]):
        for y in range(M.shape[1]):
            adj[x][y] = sign(abs(x-y)) * determinant(submatrix(M, x, y))
    return adj.astype(int)


def transpose(M: np.ndarray) -> np.ndarray:  # TODO
    N = np.zeros(shape=M.shape, dtype=int)
    for i in range(M.shape[0]):
        for j in range(M.shape[1]):
            N[i][j] = M[j][i]
    return N


def inverse_matrix(M: np.ndarray) -> np.ndarray:  # TODO
    det = determinant(M)

    if det == 0:
        raise NonInvertibleError("Determinant is 0")
    if M.shape[0] != M.shape[1] or M.shape[0] < 3:
        raise DimensionError("Dimension not correct")
    M = transpose(M)

    for i in range(M.shape[0]):
        for j in range(M.shape[1]):
            M[i][j] = M[i][j] / det

    return M


M = np.array(((4, 6, 3, 4), (4, 7, 2, 6), (3, 3, 3, 1), (3, 7, 1, 6)))
P = np.array(((3, 0, 4), (0, 1, 0), (1, 3, 3)))
N = np.array(((13, -13), (0, 17)))
Q = np.array(((1, 2, 3), (4, 5, 6), (7, 8, 9)))
print(inverse_matrix(M))
